package com.example.marci.actividad3

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {

    @override onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView textView = (TextView) findViewById(R.id.textViewer);
        Button button = (Button) findView findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener() {
            @override
            public void onClick(View view) {
                textView.setText("Marcial Fernado Tellez Hernandez");
            }
        }
    });
}
